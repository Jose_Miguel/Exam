Problem 8

public class Bits {

    static int UnsetBits(int num) {
        int unset_bits = 0, meter = 0, maximum_bits = 16;
        while (num > 0)
        {
            if ( num % 2 == 1 )
            {
                meter++;
            }
            num = num / 2;
            
        }
        unset_bits = maximum_bits - meter;
        return unset_bits;
    }
    
    public static void main(String[] args) {
        int num = 12547, unset_bits = 0;
        unset_bits = UnsetBits(num);
        System.out.println("Unset bits are: " + unset_bits);
        
       
    }
    
}

Problem 9

public class FizzBuzz{
 
     public static void main(String []args){
        int i, maximum=100, number_one = 3, number_two = 5;
        for(i=1; i <= maximum; i++)
        {
            if((i % number_one) == 0 && (i % number_two) == 0 )
            {
                System.out.println("FizzBuzz");
            }
            else
            {
                 if((i % number_one) == 0)
                {
                    System.out.println("Fizz");
                }
                 else
                 {
                    if((i % number_two) == 0)
                    {
                        System.out.println("Buzz"); 
                    }
                    else
                    {
                        System.out.println(i);
                    }
                }
            }
        }
    }
}

problem 10

public class Exam {
    public static void main(String[] args) {
        try
        {
            Calendar date = Calendar.getInstance();
             
            int current_year = date.get(Calendar.YEAR), age = 0;
            String birth_year ="2345";
            int year = Integer.parseInt(birth_year);
            int lifespan = 80;
            age = current_year - year;
            if (age > lifespan)
            {
                throw new Exception("Birth year is not real");
            }
            else
            {
                if (age < 0)
                {
                    throw new Exception("Birth year bigger than current year");
                }
            }
            System.out.println(age);
        }
        catch(NumberFormatException e){
            System.out.println("You didnt type numbers");
        }
        catch (Exception e) 
        {
            System.out.println(e.getMessage());
        }
    }
     
}

problem 11

import java.util.Arrays;

public class MyClass {
    public static void main(String args[]) {
        int array1 [] = {1,3,5,7,9};
        int array2 [] = {2,4,6,8,10};
        int array_long = array1.length + array2.length,i;
        int new_array []= new int[array_long];
        new_array = merge(array1, array2);
        
        for(i = 0; i < array_long; i++)
        {
            System.out.println(new_array[i]);
        }
    }
    
    
    
    public static int [] merge(int[] array1, int[] array2)
    {
        int array_long = array1.length + array2.length,i, j = array1.length;
        int new_array[] = new int [array_long];
        //System.out.println(array_long);
        for(i = 0; i < array_long; i++)
        {
            if(i <= j-1)
            {
                
                new_array[i] = array1[i];
            }
            else
            {
                
                new_array[i] = array2[i - j];
                
            }
            
        }
        Arrays.sort(new_array);
        return new_array;
    
    }
    
}



problem 12 Uncompleted

package problem.pkg12;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.util.ArrayList;
import org.json.simple.JSONValue;

public class Problem12 {
    public static void main(String args[]) throws java.io.IOException {
        JSONParser parser = new JSONParser();
	try {
        	Object obj = parser.parse(new FileReader("C:\\Users\\josemi2x\\Pictures\\c++\\json.json"));

		JSONObject jsonObject = (JSONObject) obj;
                
                JSONArray tags = (JSONArray) jsonObject.get("{");
		Iterator<String> iterator = tags.iterator();
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
/*
		String blog = (String) jsonObject.get("Paulina");
		System.out.println(blog);

		String temas = (String) jsonObject.get("dob");
		System.out.println(temas);
			
		long inicio = (Long) jsonObject.get("Inicio");
		System.out.println(inicio);

		JSONObject innerObject = (JSONObject) jsonObject.get("Karina");
		System.out.println(innerObject.toJSONString());
			
			// loop array
		JSONArray tags = (JSONArray) jsonObject.get("Paulina");
		Iterator<String> iterator = tags.iterator();
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
*/
	} catch (FileNotFoundException e) {
	//manejo de error
	} catch (IOException e) {
	//manejo de error
	} catch (ParseException e) {
	//manejo de error
	}


    }
   
    
}
